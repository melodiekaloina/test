package modele;
import java.lang.reflect.*;
import java.util.*;

import com.google.gson.Gson;

import connexion.EtablirConnex;
import util.*;

import java.sql.*;
@Chemin(url="dept")
public class Dept extends ObjectBdd{
	int idDept;
	String nom;
	String loc;	
	
	public Dept(){}
	
	public Dept(int idDept){
		this.idDept=idDept;
	}
	public Dept(int idDept,String nom,String loc){
		this.nom=nom;
		this.idDept=idDept;
		this.loc=loc;
	}
	public Dept(String nom,String loc){
		this.nom=nom;
		this.loc=loc;
	}
	public int getIdDept(){
		return idDept;
	}
	public void setIdDept(String idDept)throws Exception{
		this.idDept=Integer.valueOf(idDept);
	}
	public String getNom(){
		return nom;
	}
	public void setNom(String nom)throws Exception{
		this.nom=nom;
	}
	public String getLoc(){
		return loc;
	}
	public void setLoc(String loc)throws Exception{
		this.loc=loc;
	}
	@Override
	public HashMap<String,Field> getFields(){
		HashMap<String,Field> retour=new HashMap<String,Field>();
		Field[] fields=this.getClass().getDeclaredFields();
		for(Field field:fields) {
			retour.put(field.getName(), field);
		}
		return retour;
	}
	
	@Json
	@Session(name="user")
	@Chemin(url="add")	
	public ModelView save() throws Exception{
		Connection co=null;
		Statement stat=null;
		try{
			co= EtablirConnex.getConnection();
			String sql="insert into dept values ((nextval('scdept')),'"+this.nom+"','"+this.loc+"')";
			stat=co.createStatement();
			stat.executeUpdate(sql);
		}catch(Exception e){
			throw e;
		}
		finally{	
			stat.close();
			co.close();
		}
		ModelView retour=new ModelView();
		retour.setJsonResp(new Gson().toJson(this));
		return retour;
	}
	@Chemin(url="list")
	public ModelView lister() throws Exception{
		Connection co=null;
		Statement stat=null;
		try{
			co= EtablirConnex.getConnection();
			stat=co.createStatement();
			String sql="select * from dept";
			ResultSet rs=stat.executeQuery(sql);
			ArrayList<Dept> data=new ArrayList<>();
			 while(rs.next()){
				data.add(new Dept(Integer.valueOf(rs.getObject(1).toString()),rs.getObject(2).toString(),rs.getObject(3).toString()));
			}
			ModelView retour=new ModelView();
			retour.setUrl("listedept.jsp");
			HashMap<String,Object> dataretour=new HashMap<String,Object>();
			dataretour.put("listedept",(Object)data);
	
//			------------------------------------------------
			retour.setListe(dataretour);
			return retour;
		}catch(Exception e){
			throw e;
		}finally{			
			co.close();
			stat.close();
		}
	}
	@Chemin(url="delete")
	public ModelView delete() throws Exception{
		Connection co=null;
		Statement stat=null;
		try{
			co= EtablirConnex.getConnection();
			String sql="delete from dept where iddept="+this.idDept;
			stat=co.createStatement();
			stat.executeUpdate(sql);
		}catch(Exception e){
			throw e;
		}
		finally{	
			stat.close();
			co.close();
		}
		ModelView retour=new ModelView();
		retour=lister();
		return retour;
	}
}