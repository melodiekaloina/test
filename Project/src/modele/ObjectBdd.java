package modele;

import java.lang.reflect.*;
import java.util.HashMap;

public abstract class ObjectBdd {
	HashMap<String,Field> fields=new HashMap<String,Field>();
//	get all fields of current instance but they have to redefined this function to make sure it's specific for each class child
	public abstract HashMap<String,Field> getFields();
	
//	get method by name of method
	public Method getRightMethod(String todo,String name) {
		Method[] listMethod=this.getClass().getDeclaredMethods();
		String jname=name.substring(0,1).toUpperCase().concat(name.substring(1));
		for (Method method : listMethod) {
            if (method.getName().equals(todo+jname)) {
                return method;
            }
        }
		return null;
		
	}
}
