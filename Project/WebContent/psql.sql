create database orinasa;
/c orinasa postgres

create table Dept(
	idDept integer primary key,
	nom varchar(30),
	loc varchar(30)
);
create sequence scDept;
insert into dept values (nextval('scDept'),'ACCOUNTING', 'NEW YORK');
insert into dept values (nextval('scDept'),'RESEARCH', 'DALLAS');
insert into dept values (nextval('scDept'),'SALES', 'CHICAGO');
insert into dept values (nextval('scDept'),'OPERATIONS', 'BOSTON');

